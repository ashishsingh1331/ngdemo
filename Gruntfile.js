module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        src: 'js',

        watch: {
            options: {
                livereload: true,
            },
            scripts: {
                files: ["<%=src%>/app/**/*.js"],
            },
            grunt: {
                files: ["Gruntfile.js"],
                tasks: ["newer:jshint:grunt"]
            },
            html: {
                files: ["<%=src%>/app/**/*.html", "*.html"],
            },
        },
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-watch');
    // Default task(s).
    grunt.registerTask('default', ['watch']);

};
